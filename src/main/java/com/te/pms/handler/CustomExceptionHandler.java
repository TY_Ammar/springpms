package com.te.pms.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.te.pms.exception.UserNotFoundException;
import com.te.pms.response.SuccessResponse;

@RestControllerAdvice
public class CustomExceptionHandler {
	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<SuccessResponse> usernotFoundExceptionHandler(UserNotFoundException exception) {
		return new ResponseEntity<>(new SuccessResponse(true, exception.getMessage(), null), HttpStatus.BAD_REQUEST);
	}

}
