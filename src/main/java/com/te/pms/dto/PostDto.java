package com.te.pms.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
	private Long postId;
	private Long parentId;
	private String title;
	private Integer published;
	private String metaTile;
	private String slug;
	private String summary;
	private String context;
	private Long userId;

}
