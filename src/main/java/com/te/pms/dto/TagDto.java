package com.te.pms.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TagDto {
	private Long tagId;
	private String title;
	private String metaTile;
	private String slug;
	private String content;
	private Long postId;

}
