package com.te.pms.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
	private Long userId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String mobileNumber;
	private String emailId;
	private String password;
	private String intro;
	private String profile;

}
