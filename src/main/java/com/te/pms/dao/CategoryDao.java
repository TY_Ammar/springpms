package com.te.pms.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.te.pms.entity.Category;
import com.te.pms.entity.Post;

public interface CategoryDao extends JpaRepository<Category, Integer> {

	Category save(Optional<Post> findByPostId);

}
