package com.te.pms.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.te.pms.entity.Post;

public interface PostDao extends JpaRepository<Post, Long> {
	Optional<Post> findByPostId(Long postId);

}
