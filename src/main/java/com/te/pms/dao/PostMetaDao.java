package com.te.pms.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.te.pms.entity.PostMeta;

public interface PostMetaDao extends JpaRepository<PostMeta, Long> {
	Optional<PostMeta> findById(Long postId);

}
