package com.te.pms.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.te.pms.entity.User;

public interface UserDao extends JpaRepository<User, Long> {
	Optional<User> findByUserId(Long userId);

}
