package com.te.pms.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.te.pms.entity.PostComment;

public interface PostCommentDao extends JpaRepository<PostComment, Long> {

}
