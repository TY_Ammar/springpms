package com.te.pms.service;

import java.util.List;

import com.te.pms.dto.CategoryDto;

public interface CategoryService {
	public CategoryDto addCategory(CategoryDto categoryDto);

	public CategoryDto updateCategory(CategoryDto categoryDto);

	public CategoryDto deleteCategory(int id);

	public List<CategoryDto> getCategory(Long postId);

}
