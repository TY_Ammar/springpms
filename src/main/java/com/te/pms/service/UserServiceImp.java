package com.te.pms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.pms.dao.UserDao;
import com.te.pms.dto.UserDto;
import com.te.pms.entity.User;
import com.te.pms.exception.UserNotFoundException;

@Service
public class UserServiceImp implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public UserDto addUser(UserDto userDto) {
		if (userDto != null) {
			User user = new User();
			BeanUtils.copyProperties(userDto, user);
			User saveUser = userDao.save(user);
			UserDto userDto2 = new UserDto();
			BeanUtils.copyProperties(saveUser, userDto2);
			return userDto2;

		}
		throw new UserNotFoundException("user not added");
	}

	@Override
	public List<UserDto> getUser() {
		List<UserDto> userList = new ArrayList<>();
		List<User> findAll = userDao.findAll();
		if (!findAll.isEmpty()) {
			findAll.forEach(i -> {
				UserDto userDto = new UserDto();
				BeanUtils.copyProperties(i, userDto);
				userList.add(userDto);
			});
			return userList;
		}
		throw new UserNotFoundException("invalid user");
	}

	@Override
	public UserDto getUserById(Long userId) {
		Optional<User> findByUserId = userDao.findByUserId(userId);
		if (findByUserId.isPresent()) {
			UserDto userDto = new UserDto();
			BeanUtils.copyProperties(findByUserId.get(), userDto);
			return userDto;
		}

		throw new UserNotFoundException("invalid user id");
	}

	@Override
	public UserDto updateUser(UserDto userDto) {
		Optional<User> findById = userDao.findById(userDto.getUserId());
		if (findById.isPresent()) {
			BeanUtils.copyProperties(userDto, findById.get());
			userDao.save(findById.get());
			BeanUtils.copyProperties(findById.get(), userDto);
			return userDto;
		}
		throw new UserNotFoundException("invalid user");
	}

	@Override
	public UserDto deleteUser(Long userId) {
		Optional<User> findById = userDao.findById(userId);
		if (findById.isPresent()) {
			userDao.deleteById(userId);
			return new UserDto();
		}
		throw new UserNotFoundException("invalid id");
	}

}
