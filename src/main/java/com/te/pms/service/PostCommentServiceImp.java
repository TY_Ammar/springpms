package com.te.pms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.pms.dao.PostCommentDao;
import com.te.pms.dao.PostDao;
import com.te.pms.dao.UserDao;
import com.te.pms.dto.PostCommentDto;
import com.te.pms.entity.Post;
import com.te.pms.entity.PostComment;
import com.te.pms.entity.User;
import com.te.pms.exception.UserNotFoundException;

@Service
public class PostCommentServiceImp implements PostCommentService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private PostDao postDao;

	@Autowired
	private PostCommentDao postCommentDao;

	@Override
	public PostCommentDto addPostComment(PostCommentDto postCommentDto) {
		Optional<User> findByUserId = userDao.findByUserId(postCommentDto.getUserId());
		if (findByUserId.isPresent()) {
			Optional<Post> findById = postDao.findById(postCommentDto.getPostId());
			if (findById.isPresent()) {
				PostComment postComment = new PostComment();
				postComment.setPost(findById.get());
				BeanUtils.copyProperties(postCommentDto, postComment);
				postCommentDao.save(postComment);
				BeanUtils.copyProperties(postComment, postCommentDto);
				return postCommentDto;

			}
			throw new UserNotFoundException("invalid");
		}
		throw new UserNotFoundException("invalid post");
	}

	@Override
	public List<PostCommentDto> getPostComment(Long postId) {
		Optional<Post> findById = postDao.findById(postId);
		if (findById.isEmpty()) {
			throw new UserNotFoundException("post comment is not available");
		}
		Post post = findById.get();
		List<PostComment> postCommentlist = post.getPostCommentlist();
		List<PostCommentDto> postCommentDtoList = new ArrayList<>();
		postCommentlist.forEach(i -> {
			PostCommentDto postCommentDto = new PostCommentDto();
			BeanUtils.copyProperties(i, postCommentDto);
			postCommentDtoList.add(postCommentDto);
		});
		return postCommentDtoList;
	}

	@Override
	public PostCommentDto updatePostComment(PostCommentDto postCommentDto) {
		Optional<PostComment> findById = postCommentDao.findById(postCommentDto.getPostCommentId());
		if (findById.isPresent()) {
			BeanUtils.copyProperties(postCommentDto, findById.get());
			postCommentDao.save(findById.get());
			BeanUtils.copyProperties(findById.get(), postCommentDto);
			return postCommentDto;

		}
		throw new UserNotFoundException("invalid post id");
	}

	@Override
	public PostCommentDto deletePostComment(Long postCommentId) {
		Optional<PostComment> findById = postCommentDao.findById(postCommentId);
		if (findById.isPresent()) {
			postCommentDao.deleteById(postCommentId);
			return new PostCommentDto();
		}
		throw new UserNotFoundException("invalid id");
	}

}
