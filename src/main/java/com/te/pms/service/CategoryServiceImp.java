package com.te.pms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.pms.dao.CategoryDao;
import com.te.pms.dao.PostDao;
import com.te.pms.dto.CategoryDto;
import com.te.pms.entity.Category;
import com.te.pms.entity.Post;
import com.te.pms.exception.UserNotFoundException;

@Service
public class CategoryServiceImp implements CategoryService {

	@Autowired
	private PostDao postDao;

	@Autowired
	private CategoryDao categoryDao;

	@Override
	@Transactional
	public CategoryDto addCategory(CategoryDto categoryDto) {
		Optional<Category> findById2 = categoryDao.findById(categoryDto.getId());
		Post post = postDao.findById(categoryDto.getPostId()).orElseThrow(() -> new RuntimeException("Not FOund"));
		if (findById2.isEmpty()) {
			Category category = new Category();
			BeanUtils.copyProperties(categoryDto, category);
			category.setPostList(List.of(post));
			Category save = categoryDao.save(category);
//			post.getCategoList().add(save);
			return categoryDto;

		}
		throw new UserNotFoundException("post id  not found");
	}

	@Override
	public CategoryDto updateCategory(CategoryDto categoryDto) {
		Optional<Category> findById = categoryDao.findById(categoryDto.getId());
		if (findById.isPresent()) {
			BeanUtils.copyProperties(categoryDto, findById.get());
			categoryDao.save(findById.get());
			BeanUtils.copyProperties(findById.get(), categoryDto);
			return categoryDto;

		}
		throw new UserNotFoundException("invalid id");
	}

	@Override
	public CategoryDto deleteCategory(int id) {
		Optional<Category> findById = categoryDao.findById(id);
		if (findById.isPresent()) {
			categoryDao.deleteById(id);
			return new CategoryDto();

		}
		throw new UserNotFoundException("invalid id");
	}

	@Override
	public List<CategoryDto> getCategory(Long postId) {
		Optional<Post> findById = postDao.findById(postId);
		if (findById.isEmpty()) {
			throw new UserNotFoundException("invalid id");

		}
		Post post = findById.get();
		List<Category> categoList = post.getCategoList();
		List<CategoryDto> categoryDtoList = new ArrayList<>();
		categoList.forEach(i -> {
			CategoryDto categoryDto = new CategoryDto();
			BeanUtils.copyProperties(i, categoryDto);
			categoryDtoList.add(categoryDto);
		});
		return categoryDtoList;
	}
}
